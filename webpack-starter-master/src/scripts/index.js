import '../styles/index.scss';
import * as moment from 'moment';

console.log('webpack starterkit');
console.log(moment().format('LLL'));
require("moment");

window.onload = function(){
    // Task 3 - Clock
    updateClock();
    setInterval(updateClock, 1000);
};

function updateClock(){
    document.getElementById("clock").innerHTML = moment().format("HH:mm:ss");
}

// Task 4 - Calculator
// Clear the output whenever the calculate method has been successfully run
var clearOutput = false;

// Only one operator is allowed.
var isThereAnOperator = false;
var output = document.getElementById("calcOutput");

function addNumber(num){
    if(clearOutput) {
        output.value = "";
        clearOutput = false;
    }
    output.value += num;
}

function addModifier(mod){
    if(clearOutput) {
        output.value = "";
        clearOutput = false;
    }
    if(!isThereAnOperator){
        if(output.value.length > 0){
            output.value += mod;
            isThereAnOperator = true;
        }
    }
}

function removeLast(){
    var t = output.value;
    var lastChar = t.substring(t.length - 1);
    if(lastChar == '+' || lastChar == '*' || lastChar == '-' || lastChar == '/'){
        isThereAnOperator = false;
    }
    t = t.substring(0, t.length - 1);
    output.value = t;
}

function calculate(){
   output.value = Math.round(eval(output.value) * 100) / 100;
   clearOutput = true;
   isThereAnOperator = false;
}

// Task 5 - Rock paper shotgun
var gameRunning = true;
var playerChoice = null;
var tie = "This is a tie";
var win = "You win";
var lose = "You lose";
var result = [
    [tie, win, lose],
    [lose, tie, win],
    [win, lose, tie]
];

function playRound(choice) {
    if(!gameRunning) {
        return;
    }
    playerChoice = choice;
    shakeHand(choice);
    loadBoard();
    gameRunning = false;
}

// Displays the board
function displayBoard(){
        let cpuChoice = Math.floor((Math.random() * 3));
        
        // Rotate all player images by 90 degrees
        document.getElementById("playerImgRock").style = "transform: rotate(90deg)";
        document.getElementById("playerImgPaper").style = "transform: rotate(90deg)";
        document.getElementById("playerImgScissor").style = "transform: rotate(90deg)";

        // Hide the player's options, except the one he clicked on
        if(playerChoice == 0){
            document.getElementById("playerRock").style = "display:inline";
            document.getElementById("playerPaper").style = "display:none";
            document.getElementById("playerScissor").style = "display:none";
        }
        else if(playerChoice == 1){
            document.getElementById("playerPaper").style = "display:inline";
            document.getElementById("playerRock").style = "display:none";
            document.getElementById("playerScissor").style = "display:none";
        }
        else {
            document.getElementById("playerScissor").style = "display:inline";
            document.getElementById("playerPaper").style = "display:none";
            document.getElementById("playerRock").style = "display:none";
        }
    
        // Displays the image for the cpu's choice
        if(cpuChoice == 0){
            document.getElementById("cpuChoice").src = "src/img/rock.png";
            document.getElementById("cpu").style = "display:inline-block";
        }
        else if(cpuChoice == 1){
            document.getElementById("cpuChoice").src = "src/img/paper.png";
            document.getElementById("cpu").style = "display:inline-block";
        }
        else {
            document.getElementById("cpuChoice").src = "src/img/scissor.png";
            document.getElementById("cpu").style = "display:inline-block";
        }

        document.getElementById("cpuChoice").hidden = false;
        document.getElementById("cpuChoice").style = "transform: scaleX(-1) rotate(90deg)";

        document.getElementById("restart").hidden = false;
        document.getElementById("result").innerHTML = result[cpuChoice][playerChoice];
}

// Hides the player's choice, and shows the loading animation
function loadBoard(){
    document.getElementById("playerRock").style = "display:none";
    document.getElementById("playerPaper").style = "display:none";
    document.getElementById("playerScissor").style = "display:none";
    document.getElementById("cpu").style = "display:inline-block";
    document.getElementById("player").style = "margin-right: 100px";
}

function restartGame(){
    document.getElementById("cpu").style = "display:none";
    document.getElementById("cpuChoice").hidden = true;
    document.getElementById("playerRock").style = "display:inline";
    document.getElementById("playerPaper").style = "display:inline";
    document.getElementById("playerScissor").style = "display:inline";
    document.getElementById("playerImgRock").style = "transform: rotate(0deg)";
    document.getElementById("playerImgPaper").style = "transform: rotate(0deg)";
    document.getElementById("playerImgScissor").style = "transform: rotate(0deg)";
    document.getElementById("restart").hidden = true;
    document.getElementById("result").innerHTML = "";
    document.getElementById("player").style = "margin-right: auto";
    gameRunning = true;
}

// Shakes the hand and calls the displayBoard method when done.
function shakeHand(){
    var player = document.getElementById("playerShake");
    var cpu = document.getElementById("cpuShake");
    
    player.hidden = false;
    cpu.hidden = false;
    
    var pos = 0;
    var posR = 0;
    var interval = 0;
    
    var id = setInterval(frame, 1);

    function frame() {
        if (pos == 180) {
            if(interval == 2){
                clearInterval(id);
                displayBoard();
                
                player.hidden = true;
                cpu.hidden = true;
            } else {
                pos = 0;
                posR = 0;
                interval++;
            }
        } else if(pos <= 90) {
            player.style = "transform: rotate("+ pos +"deg)";
            cpu.style = "transform: scaleX(-1) rotate("+ pos +"deg)";
        } else {
            if(interval != 2){
                player.style = "transform: rotate("+ (pos - posR) +"deg)";
                cpu.style = "transform: scaleX(-1) rotate("+ (pos - posR) +"deg)";
                posR += 2;
            }
        }
        pos++;
    }
}

// This is stupid
global.playRound = playRound;
global.addNumber = addNumber;
global.addModifier = addModifier;
global.removeLast = removeLast;
global.calculate = calculate;
global.restartGame = restartGame;